% Created 2015-05-04 lun. 01:07
\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\usepackage{minted}
\usepackage{hyperref}
\usepackage[frenchb, english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsthm,mathtools,amsmath}
\usepackage{tikz}
\usetikzlibrary{positioning,shapes,shadows,arrows,automata}
\usepackage{multicol}
\newtheorem{mydef}{Définition}
\newtheorem{exercice}{Exercice}
\newtheorem{prop}{Proposition}
\newtheorem{ex}{Exemple}
\newtheorem{theor}{Théorème}
\newtheroem{prop}{Propriété}
\tikzset{every initial by arrow/.style={initial text=,->,very thick}}
\date{}
\title{Résumé Principe d'Interprétation des Langages}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 24.4.1 (Org mode 8.2.10)}}
\begin{document}

\maketitle
\tableofcontents


\section{Introduction}
\label{sec-1}

En informatique on utilise de nombreux langages, des langages de programmation (C++, OCaml, Python, \ldots{}), mais aussi
des langages de description (HTML, XML, PDF, JSON, \ldots{}), etc. \\

Un informaticien est amené à écrire des programmes pour analyser, interpréter des données écrites dans un lanage donné,
par exemple pour les traduires en un autre langage ou les mettre en entrée d'un logiciel existant. \\

\subsection{Ce qui est vu en cours}
\label{sec-1-1}

Concepts théoriques:
\begin{itemize}
\item bases de la théorie des langages formels
\item un peu de sémantique des langages de programmation
\end{itemize}

Des outils pratiques:
\begin{itemize}
\item Lex pour l'analyse lexicale
\item Yacc pour l'analyse syntaxique
\end{itemize}

\subsection{Exemple emblématique: la compilation des programmes}
\label{sec-1-2}

Un compilateur prend en entrée un programme écrit dans un langage de haut niveau et le traduit
\begin{itemize}
\item soit en langage machine
\item soit en langage intermédiaire (cas des langages "semi-compilés", comme Java)
\end{itemize}

\subsection{Principales phases de la compilation d'un programme}
\label{sec-1-3}


\begin{tikzpicture}

\node[rectangle] (source) {Programme source};

\node[rectangle,draw,below=of source] (analyse-lex) {Analyse lexicale};

\draw[->,color=blue] (source) -- (analyse-lex);

\node[rectangle, draw, below=of analyse-lex] (analyse-syntax) {Analyse syntaxique};

\draw[->,color=blue] (analyse-lex) -- (analyse-syntax);

\node[rectangle, draw, below=of analyse-syntax] (analyse-seman) {Analyse sémantique};

\draw[->,color=blue] (analyse-syntax) -- (analyse-seman);

\node[rectangle, draw, below=of analyse-seman] (gener-code) {Génération de code};

\draw[->,color=blue] (analyse-seman) -- (gener-code);

\node[rectangle, draw, below=of gener-code] (opt-code) {Optimisation de code};

\draw[->,color=blue] (gener-code) -- (opt-code);

\node[rectangle, below=of opt-code] (prog-cible) {Programme cible};

\draw[->,color=blue] (opt-code) -- (prog-cible);

\end{tikzpicture}

Analyse lexicale:
\begin{itemize}
\item reconnaître le "statut" de chaque mot lu dans le programme
\item détermine la suite des unités lexicales (ou tokens) du programme
\item outil lex permet de faire de l'analyse lexicale (mais pas seulement)
\end{itemize}

Analyse syntaxique:
\begin{itemize}
\item vérifier que les unités lexicales sont dans le bon ordre
\item ordre défini par la grammaire du langage
\item résultat de l'analyse est un arbre syntaxique
\item outil yacc permet de faire de l'analyse syntaxique (mais pas seulement)
\end{itemize}

Analyse sémantique:
\begin{itemize}
\item vérifie si le programme (syntaxiquement correct) "a un sens"
\item notamment, on vérifie que les règles de typage sont vérifiées
\end{itemize}

\section{Notions de base sur les langages}
\label{sec-2}
\subsection{Alphabets et mots}
\label{sec-2-1}

\begin{mydef}
Un alphabet est un ensemble fini arbitraire, dont les éléments sont appelés symboles.
\end{mydef}

On dit aussi parfois vocabulaire pour alphabet, et dans certains cas particuliers les symboles sont appelés
lettres ou encore caractères. Exemples: l'alphabet latin, le jeu de caractères ASCII. \\

\begin{mydef}
Soit $A$ un alphabet. Un mot sur $A$ est une séquence finie de symbole de $A$.
\end{mydef}

Les mots seront en général notés par $u$, $v$, ou $w$. On ecrira $w = a_1 \ldots a_n$ (en juxtaposant les lettres)
pour dire que $w$ désigne la séquence $a_1, \ldots, a_n$. \\

\begin{mydef}
Le mot vide est le mot formé de la séquence vide de symboles. On le note $\epsilon $. La concaténation est l'opération
de type $mot \times mot \rightarrow mot$, notée par "$\cdot$" telle que si $u = a_1 \ldots a_n$ et $v = b_1 \ldots b_k$
alors $u \cdot v = a_1 \ldots a_n b_1 \ldots b_k$
\end{mydef}


La concaténation n'est pas une opération commutative: on n'a pas toujours $u \cdot v = v \cdot u$. Par
exemple, sur l'alphabet $\{a,b\}, a \cdot b \neq b \cdot a$. Par contre, la concaténation est associative : pour tous
mots $u, v$ et $w$, $(u \cdot v) \cdot w = u \cdot (v \cdot w)$; et on a également pour tout mot $u$, $\epsilon u =
i \cdot \epsilon = u$. \\

\begin{mydef}
La longueur est l'opération de type $mot \rightarrow entier$, la longueur de $w$ étant notée $|w|$, telle que si $w =
a_1 \cdot a_n$ alors $|w| = n$.
\end{mydef}

L'exponentiation est l'opération de type mot \texttimes{} eniter $\rightarrow$ mot, notée par la notation puissance habituelle,
définie par: $w^n = \underbrace{w \cdot w \ldots w}_{n fois}$

\subsection{Propriétés de la longueur et de l'exponentiation}
\label{sec-2-2}
$|\epsilon| = 0$ \\
$|u \cdot v| = |u| + |v|$ \\
$w^0 = \epsilon$ \\
$w^{n+1} = w \cdot w^n = w^n \cdot w$ \\

\subsection{Langage}
\label{sec-2-3}

Soit $A$ un alphabet fixé.

\begin{mydef}
Un langage sur A est un ensemble de mots sur A.
\end{mydef}

\subsubsection{Questions:}
\label{sec-2-3-1}

\begin{exercice}
Sur l'alphabet $A = \{a, b\}$, considérons les mots $w_1 = abab, w_2 = aaabab, w_3 = babaa, w_4 = \epsilon$ et $w_5 = aba$;
et les langages \\

\begin{itemize}
\item $L1$: mots commençant par $ab$;
\item $L2$: mots se terminant par $aa$;
\item $L3$: mots contenant une occurence d'au moins trois $a$ consécutifs;
\item $L4$: mots ne contenant pas d'occurences de $bab$;
\item $L5$: mots de longueur impaire commençant par $ab$ et finissant par $ba$.
\end{itemize}

\begin{enumerate}
\item Pour chaque $i$ et $j$, dire si le mot $w_i$ appartient au langage $L_i$.
\item Trouver un mot $w_6$ qui appartient à $L_i$ si et seulement si $i$ est pair, et un mot $w_7$ qui appartient à $L_i$
si et seulement si $i$ est impair (trouver les mots les plus courts possibles).
\end{enumerate}

\end{exercice}

\subsubsection{Réponses:}
\label{sec-2-3-2}

\begin{enumerate}
\item 
\end{enumerate}

\begin{center}
\begin{tabular}{llllll}
\hline
 & $L_1$ & $L_2$ & $L_3$ & $L_4$ & $L_5$\\
\hline
$w_1$ & oui & non & non & non & non\\
$w_2$ & non & oui & oui & non & non\\
$w_3$ & non & non & non & non & non\\
$w_4$ & non & non & non & oui & non\\
$w_5$ & oui & non & non & oui & oui\\
\hline
\end{tabular}
\end{center}

\begin{enumerate}
\item 

\item $aa$

\item $ababa$
\end{enumerate}

\subsection{Opérations sur les langages}
\label{sec-2-4}

\begin{mydef}
Le langage vide $\emptyset$ est le langage qui ne contient aucun mot. L'union de deux langages $L_1$ et $L_2$ est le
langage $L_1 \cup L_2$ contenant les mots de $L_1$ et les mots de $L_2$. Leur intersection $L_1 \cap L_2$ est le langage
formé des mots qui ne sont à la fois dans $L_1$ et dans $L_2$
\end{mydef}

Ne pas confondre le langage vide avec le langage $\{\epsilon\}$ qui contient uniquement le mode vide.

\begin{mydef}
La concaténation de deux langages $L_1$ et $L_2$ est le langage $L_1 \cdot L_2$ formé par toutes les concaténations
possibles d'un mot de $L_1$ et un mot de $L_2$:
$L_1 \cdot L_2 = \{w_1 \cdot w_2 | w_1 \in L_1, w_2 \in L_2\}$
L'exponentiation d'un langage $L$ est définie par $L^n = \underbrace{L \cdot L \ldots L}_{n \text{fois}}$
c'est à dire $L^n = \{w_1 \cdot w_2 \ldots w_n | w_i \in L\}$
\end{mydef}

La concaténation de langages est également associative mais pas commutative. 
Notons que $L^n$ n'est pas $\{w^n | w \in L\}$.

\begin{mydef}
L'itération d'un langage $L$ est le langage $L^*$ formé par les concaténations d'un nombre quelconque de mots de $L$:
$L^{*} = \{w_1 \cdot w_2 \ldots w_k | k \in \mathbb{N}, w_i \in L\}$ 
Autrement dit: $L^{*} = \bigcup_{k \in \mathbb{N}} L^k$ 
\end{mydef}

L'opération $*$ est encore appelée "étoile de Kleene".
Ex: $A^*$ contient tout les mots possibles de $A$.

\subsubsection{Questions}
\label{sec-2-4-1}

Soit $A = \{a, b\}$, soient $L_1 = \{aa, ab\}$, $L_2 = \{a, aba\}$, quels sont les langages suivants?
\begin{itemize}
\item $L_1^2, L_1 \cdot L_2, L_2^2, L_1^2 \cap L_2^2$
\item $L_1 \cap \emptyset, L_1 \cup \emptyset, L_1 \cdot \emptyset$
\item $L_1^*$
\end{itemize}

\subsubsection{Réponses}
\label{sec-2-4-2}

\section{Expressions rationnelles (ou régulières)}
\label{sec-3}
Soit $A$ un alphabet fixé.

\begin{mydef}
Une expression régulière (ou expression rationelle) sur $A$ est une notation qui décrit un langage sur $A$. Ces notations sont
\begin{enumerate}
\item $\epsilon$ : décrit le lanage $\{\epsilon\}$
\item $w$ mot sur $A$ : décrit le langage $\{w\}$
et si $e$, $e_1$, $e_2$ sont déjà des expressions régulières désignant respectivement les langages $L, L_1$ et $L_2$:
\item $e_1$ | $e_2$ : décrit le langage $L_1 \cup L_2$
\item $e_1e_2$ : décrit le lanage $L_1 \cdot L_2$
\item $e*$ : décrit le langage $L^{*}$
\item $(e)$ : décrit aussi le lanage $L$
\end{enumerate}
\end{mydef}

\begin{ex}

Sur l'alphabet des caractères ASCII l'expression

$ 0 | ((1|2|3|4|5|6|7|8|9) ((0|1|2|3|4|5|6|7|8|9)*))$

décrit le lanage des constantes entières non signées en décimal

\end{ex}

\subsubsection{Exercice}
\label{sec-3-0-1}

Enumérer tout les mots de longueur inférieur ou égale à 3 des langages suivants:
\begin{itemize}
\item (a|b|c|d)*
\item (a|b)*(c|d)
\item (a|b)*(c|d)*
\item (a*|c)*
\end{itemize}


Donner une expression rationelle pour chacun des langages suivants sur l'alphabet $A={a,b,c}$:
\begin{itemize}
\item l'ensemble des mots qui commencent par "abc"
\item l'ensemble des mots qui ne contiennent pas de "b"
\item l'ensemble des mots qui commencent par "abc" ou par "ac"
\item l'intersection des deux langages précédents
\end{itemize}

Pour chacun des mots et chacune des expressions rationnelles suivants, déterminer si les mot appartient au langage défini
par l'expression

\begin{center}
\begin{tabular}{ll}
Expressions rationnelles & Mots\\
\hline
$e_1 = aa \vert b^{*}$ & $w_1 = \epsilon$\\
$e_2 = (aa \vert b)^{*}$ & $w_2 = abba$\\
$e_3 = (ab \vert a)^{*}a$ & $w_3 = a^{10}$\\
$e_4 = a(ba \vert a)^*$ & $w_4 = b^7$\\
$e_5 = (a^*)^*$ & $w_5 = aaabbb$\\
$e_6 = (a^*b^*)^*$ & $w_6 0 abbbabbabbabababbabababa$\\
$e_7 = (a\vert b)^{*}$ & \\
$e_8 = aab(a^* \vert b^*)baa \vert baa^*$ & \\
\end{tabular}
\end{center}

\subsection{Langages rationnels (ou réguliers)}
\label{sec-3-1}

\begin{mydef}
Un langage est dit régulier (ou rationnel) s'il existe une expression régulière qui le décrit
\end{mydef}

Il existe des langages qui ne sont pas réguliers, par exemple $\{a^n b^n | n \in \mathbb{N}\}$

\subsection{Expression rationnelles étendues}
\label{sec-3-2}

\begin{mydef}
Les expressions régulières étendues sont obtenus en complétant les notations par
\begin{enumerate}
\item $e+$ : décrit le langage $L \cdot L^* = \bigcup_{k > 0} L^k$
\item $e?$ : décrit le langage $L \cup \{\epsilon \}$
\item $[a_1 \ldots a_n]$ où les $a_i$ sont des symboles de $A$ : décrit le langage des $n$ mots à une lettre
$\{a_1, \ldots, a_n\}$
\item $[^a_1 \ldots a_n]$ où les $a_i$ sont des symboles de $A$ : décrit le langage des mots à une lettre
$\{b | b \in A, b \neq a_i \text{pour tout i} \}$
\end{enumerate}
On peut remplacer la notation $[a1 \ldots a_n]$ et $[^a_1 \ldots a_n]$ par des intervalles $[a_1-a_n]$ et $[^a_1 \ldots a_n]$ pour
des $a_1 \ldots a_n$ qui ont un sens, c'est à dire des chiffres, ou tout des lettres minuscules ou tout des lettres majuscules
\end{mydef}

\begin{ex}

Sur l'alphabet des caractères ASCII l'expression étendue $[0123456789]+$ ou bien $[0-9]+$ décrit le langage des constantes
entières non signées en décimal, où l'on autorise les zéros superflus au début. L'expression étendue $"[^"]*"$ décrit le
langage des chaînes de caractères d'un langage de programmation (comme CAML, Java ou C).

\end{ex}

\begin{prop}
Tout langage désigné par une expression étendue est régulier, autrement dit il peut aussi être désigné par une expression régulière "de base".
\end{prop}

\section{Automates finis}
\label{sec-4}

Un automate fini est un quintuplet $\mathcal{A} = (A, Q, q_0, F, \delta)$, où:
\begin{itemize}
\item $A$ est un alphabet (fini)
\item $Q$ est un ensemble fini, appelé l'ensemble des états
\item $q_0$ est l'état initial, $q_o \in Q$
\item $F$ est l'ensemble des états terminaux $F \subset Q$
\item $\delta$ est la fonction de transition: $\delta : (Q \times (A \cup \{\epsilon\})) \rightarrow 2^{Q}$ où $2^{Q}$
\end{itemize}
désigne l'ensemble des parties de $Q$

\begin{tikzpicture}

\node[initial, state] (0) {0};
\node[state, right=of 0] (1) {1};
\node[state, above right=of 1] (2) {2};
\node[accepting, state, right =of 2] (3) {3};
\node[state, below right =of 1] (2') {2'};
\node[accepting, state, right = of 2'] (3') {3'};

\path[->] 
(0) edge node[below] {c} (1)
(1) edge node[below] {a} (2)
    edge node[below] [swap] {a} (2')
(2) edge node[below] {r} (3)
(2') edge node[below] {r} (3');

\node[below=of 0] (init) {Etat initial};
\node[below=of 3'](fini) {Etats finaux};

\end{tikzpicture}

\begin{itemize}
\item $A = \{a, c, l, r\}$
\item $Q = \{0, 1, 2, 3, 2', 3'\}$
\item $q_0 = 0$
\item $F = \{3, 3'\}$
\end{itemize}

Fonction de transition
\begin{itemize}
\item $\delta (0, c) = \{1\}$
\item $\delta (1, a) = \{2, 2'\}$
\item $\delta (2, 3) = \{3\}$
\item $\delta (2', l) = \{3'\}$\\
\end{itemize}
ou ensemble de transitions
\begin{itemize}
\item $\delta = \{(0,c,1),(1,a,2),(1,a,2'),(2,r,3),(2',l,3')\}$
\end{itemize}


\begin{tabular}{|c|c|c|c|c|}
\hline
$\delta$ transitions & a & c & l & r \\
\hline
0 & - & 1    & - & - \\
\hline
1 & 2,2' & - & - & - \\
\hline
2 & - & -    & - & 3 \\
\hline
3 & - & -    & - & - \\
\hline
2'& - & -    & 3'& - \\
\hline
3'& - & -    & - & - \\
\hline
\end{tabular}

\subsection{Automate fini}
\label{sec-4-1}

\begin{tikzpicture}

\node[state, initial] (0) {0};
\node[state, below right=of 0] (1) {1};
\node[state, above right=of 0] (2) {2};
\node[state, right=of 2] (3) {3};
\node[state, accepting, right =of 3] (4) {4};

\path[->]
(0) edge node[below] {c} (1)
(1) edge [loop below] node {g} ()
(1) edge node[below] {a} (2)
(1) edge node[below] {a} (3)
(1) edge node[below] {a} (4)
(2) edge node[below] {c} (0)
(2) edge node[below] {g} (3)
(3) edge node[below] {t} (4)
(4) edge [loop below] node[below] {c,t} ();

\end{tikzpicture}

\begin{itemize}
\item $A = \{a, c, g, t\}$
\item $Q = \{0, 1, 2, 3, 4\}$
\item $q_0 = \{0\}$
\item $F = \{4\}$
\end{itemize}

\begin{tabular}{|c|c|c|c|c|}
\hline
$\delta$ transitions & a & c & g & t \\
\hline
0 & - & 1 & - & - \\
\hline
1 & 2,3,4 & - & 1 & - \\
\hline
2 & - & 0 & 3 & - \\
\hline
3 & - & 0 & 3 & - \\
\hline
4 & - & 4 & - & 4 \\
\hline
\end{tabular}

\begin{tikzpicture}

\node[state, initial] (0) {0};
\node[state, above right =of 0] (1) {1};
\node[state, below right =of 0] (2) {2};
\node[state, accepting, below right=of 1] (3) {3};

\path[->]
(0) edge node[below] {a} (1)
(1) edge node[below] {c} (3)
(0) edge node[below] {c} (2)
(2) edge node[below] {g} (3)
(1) edge node[right] {$\epsilon$} (2);

\end{tikzpicture}


\begin{itemize}
\item $A = \{a,c,g\}$
\item $Q = \{0, 1, 2, 3\}$
\item $q_0 = 0$
\item $F = \{3\}$
\end{itemize}

\begin{tabular}{|c|c|c|c|c|}
\hline
$\delta$ transitions & a & c & g & $\epsilon$ \\
\hline
0 & 1 & 2 & - & - \\
\hline
1 & - & 3 & - & 2 \\
\hline
2 & - & - & 3 & - \\
\hline
3 & - & - & - & - \\
\hline
\end{tabular}

\subsection{Automates et langages}
\label{sec-4-2}

\begin{itemize}
\item Un automate fini définit un langage
\item Ce langage correspond à l'ensemble des chemins allant de l'état initial jusqu'à un état final
\end{itemize}

\subsubsection{Chemin dans un automate}
\label{sec-4-2-1}

\begin{mydef}
Un chemin dans un automate $\mathcal{A}$ est un mot $c \in \delta^*$ ($\delta$ est vu comme un ensemble) tel que
$c = (q_1, x_1, q_2) (q_2, x_2, q_3) ... (q_n, x_n, q_{n+1})$\\
On dit que le chemin mène de l'état $q_1$ à l'état $q_{n+1}$ \\
Le nombre $n$ est la longueur de chemin \\
\end{mydef}

\subsubsection{Trace d'un chemin}
\label{sec-4-2-2}

\begin{mydef}
On appelle trace l'homomorphisme $t : \delta^* \rightarrow A^*$ tel que $t(q,x,q') = x \forall (q,q') \in Q, \forall x
\in A \cup \{\epsilon\}$
\end{mydef}

\subsubsection{Langage reconnu par un automate}
\label{sec-4-2-3}

\begin{mydef}
Un mot $u$ est accepté (ou reconnu) par un automate fini $\mathcal{A}$ s'il existe un chemin $c$ qui mène de l'état initial
de $\mathcal{A}$ à un état final tel que $t(c)=u$
\end{mydef}


\begin{mydef}
Le langage reconnu par $\mathcal{A}$, noté $L(\mathcal{A})$ est l'ensemble des mots reconnus par $\mathcal{A}$.
\end{mydef}

\begin{mydef}
Un langage $L$ est reconnaissable s'il existe un automate fini $\mathcal{A}$ tel que $L=L(\mathcal{A})$
\end{mydef}

\subsubsection{Questions}
\label{sec-4-2-4}

Donner un automate fini pour chacun des langages suivants sur l'alphabet $A=\{a,b,c\}$:
\begin{itemize}
\item L'ensemble des mots qui commencent par "abc"
\item L'ensemble des mots qui ne contiennent pas de "b"
\item L'ensemble des mots qui commencent par "abc" ou par "ac"
\item L'ensemble des mots contenant au moins trois occurences successives de "a"
\item L'ensemble des mots de longueur paire
\end{itemize}

\subsection{Théorème de Kleene (1956)}
\label{sec-4-3}

\begin{itemize}
\item La classe des langages reconnaissable et celle des langages rationnels n'en font qu'une
\item En d'autres termes, pour tout langage défini par une expression rationnelle on peut définir un automate qui le reconnaît,
\end{itemize}
et réciproquement

\subsection{Automate fini déterministe (a.f.d)}
\label{sec-4-4}

\begin{mydef}
Un automate fini est déterministe si:
\begin{itemize}
\item il ne contient pas d'$\epsilon$-transition
\item et, $\forall q \in Q, \forall x \in A$ il existe au plus un $q' \in Q$ tel que $(q,x,q') \in \delta$.
\end{itemize}
\end{mydef}


La fonction de transition d'un automate déterministe peut être vue ainsi: $\delta : (Q \times A) \rightarrow Q$

\begin{mydef}
Un facteur d'un mot est une suite de lettres successives dans ce mot. (Ex: abc est facteur du mot cbabccb mais pas de cabbcb)
\end{mydef}

\begin{mydef}
Un sous-mot d'un mot est une suite de lettres dans ce mot, dans l'ordre mais pas forcément successives.
(Ex: abc est sous-mot du mot cbabcb et du mot cabbcb)
\end{mydef}

\subsubsection{Questions}
\label{sec-4-4-1}

\begin{itemize}
\item Donner un automate déterministe pour le langage des mots sur $\{a,b,c\}$ qui contiennent au moins une fois le facteur $abc$
\end{itemize}

\subsection{Automate fini déterministe complet}
\label{sec-4-5}

\begin{mydef}
Un automate fini déterministe est complet si $\delta(q,x)$ est défini pour tout état $q$ et toute lettre $x$
\end{mydef}

On peut lire toute lettre depuis tout état

Tout automate fini déterministe non complet peut être complété en reconnaissant le même langage en ajoutant
état "puits", et on crée toutes les transitions manquantes en les faisant pointer vers le puits

\begin{multicols}{2}

\begin{tikzpicture}

\node[state,initial] (1) {1};
\node[state, right=of 1] (2) {2};
\node[state, accepting, below left=of 2] (3) {3};

\path[->] (1) edge node[below] {a} (2);
\path[->] (2) edge node[above] {c} (1);
\path[->] (3) edge node[below] {b} (1);
\path[->] (2) edge node[below] {a} (3);
\path[->] (2) edge [loop below] node[below] {b} ();

\end{tikzpicture}

\columnbreak

\begin{tikzpicture}

\node[state,initial] (1) {1};
\node[state, right=of 1] (2) {2};
\node[state, accepting, below left=of 2] (3) {3};
\node[state, left=of 3] (4) {4};

\path[->] (1) edge node[below] {a} (2);
\path[->] (2) edge node[above] {c} (1);
\path[->] (3) edge node[below] {b} (1);
\path[->] (2) edge node[below] {a} (3);
\path[->] (2) edge [loop below] node[below] {b} ();
\path[->] (3) edge node[below] {a,c} (4);
\path[->] (1) edge node[above] {b,c} (4);
\path[->] (4) edge [loop below] node[below] {a,b,c} ();

\end{tikzpicture}

\end{multicols}

\subsection{Déterminisation d'un automate}
\label{sec-4-6}

\begin{theor}
Pour tout automate fini non déterministe, il existe un automate fini déterministe équivalent (= qui reconnait le même langage)
\end{theor}

\subsubsection{Algorithme de déterminisation}
\label{sec-4-6-1}

\begin{itemize}
\item Etape 1: Calculer l'$\epsilon$-clôture (ou orbite) de chaque état.
\end{itemize}

L'$\epsilon$-clôture d'un état $q$ est l'ensemble des états qui peuvent être atteints à partir de $q$ en ne
faisant que des $\epsilon$-transitions auquel on ajoute $q$ lui-même


\begin{itemize}
\item Etape 2: Créer la table de transition du nouvel automate comme suit:
\item Partir de l'$\epsilon$-clôture de l'état initial. Ce sera l'état initial du nouvel automate
\item Pour chaque lettre, ajouter dans la table les états produits, avec leurs $\epsilon$-clôtures. Chacun de ces ensembles
\end{itemize}
d'états devient un état dans le nouvel automate.
\begin{enumerate}
\item Recommencer 2 à partir de chaque nouvel état produit, jusqu'à ce qu'on ne crée plus de nouvel état
\item Tout les états contenant au moins un état terminal deviennent terminaux
\item Renommage éventuelle des états pour plus de clarté
\end{enumerate}

\subsubsection{Questions}
\label{sec-4-6-2}

\subsection{Propriété de clôtures des langages reconnaissables}
\label{sec-4-7}

\begin{itemize}
\item Si un langage $L$ est reconnaissable alors
\begin{itemize}
\item son complémentaire est reconnaissable
\item $L^*$ est reconnaissable
\end{itemize}

\item Si deux langages $L_1$ et $L_2$ sont reconnaissables, alors
\begin{itemize}
\item leur concaténation est reconnaissable
\item leur union, leur intersection sont reconnaissables
\end{itemize}
\end{itemize}

\subsubsection{Questions}
\label{sec-4-7-1}

\begin{itemize}
\item Montrez comment, à partir d'un automate fini de $L$, on peut construire
\begin{itemize}
\item un automate fini de son complémentaire
\item un automate fini de $L^*$
\end{itemize}

\item Montrez comment, à partir d'automates finis de $L_1$ et $L_2$ on peut construire
\begin{itemize}
\item un automate de leur concaténation
\item un automate de leur union
\item un automate de leur intersection
\end{itemize}
\end{itemize}

Soit l'alphabet $A = \{a, b, c\}$

\begin{itemize}
\item Construire un automate fini déterministe pour le langage $L_1$ des mots de $A^*$ qui contiennent au moins
\end{itemize}
une fois le facteur abc

\begin{itemize}
\item Construire un automate fini déterministe pour le langage $L_2$ des mots de $A^*$ dont le nombre de lettres $a$ est pair

\item Construire un automate déterministe pour l'intersection de $L_1$ et $L_2$
\end{itemize}

\subsection{Automate minimale}
\label{sec-4-8}

\begin{mydef}
Un automate minimal pour un langage $L$ est un automate fini déterministe qui contient le plus petit nombre d'états possible.
\end{mydef}

\begin{theor}
Pour tout langage reconnaissable $L$ il existe un unique automate minimal (à renommage des états près) qui reconnait $L$.
\end{theor}

\subsection{Un problème naturel}
\label{sec-4-9}

\begin{itemize}
\item Reconnaisance d'un mot:
\begin{itemize}
\item données: un mot $w$, un langage rationnel $L$ (donné par une expression rationnelle)
\item Question: $w \in L$ ?
\end{itemize}

\item Méthode de résolution
\begin{itemize}
\item Construire un automate fini du langage à partir de l'expression rationnelle
\item Lire le mot lettre à lettre en parcourant l'automate
\end{itemize}
\end{itemize}

\section{Bases de lex}
\label{sec-5}
\subsection{Analyse lexicale}
\label{sec-5-1}

\begin{itemize}
\item L'analyse lexicale consiste à déterminer le "statut" de chaque mot, c'est à dire l'unité lexicale (ou token, ou lexème)
\end{itemize}
qui lui correspond

\begin{itemize}
\item Les unités lexicales sont généralement définies par des expressions rationnelles

\item Le problème de base à résoudre est donc:
+Données: un mot $w$, un langage rationnel $L$ (donné par une expression rationnelle)
+Question: $w \in L$ ?

\item Problème de l'analyse lexicale:
\begin{itemize}
\item Données:
\begin{itemize}
\item un (long) mot $w$ (= le texte à analyser: programme, fichier de description, séquence génomique, \ldots{})
\item un ensemble de langages, $L_1$, $L_2$, \ldots{}, $L_k$ définis par des expressions rationnelles
\end{itemize}

\item Problème: décomposer $w$ en une concaténation de mots $w_1w_2...w_n$ de sorte que chacun des $w_i$ appartient à un $L_j$.
\end{itemize}

\item Si une telle décomposition n'est pas possible, le texte est lexicalement incorrect

\item Si la décomposition est possible, l'analyseur effectuera une ou des actions pour chaque $w_i$ (La décomposition doit être unique.
\end{itemize}
\subsubsection{Méthode}
\label{sec-5-1-1}

\begin{itemize}
\item Idée générale: construire un automate fini à partir de l'expression rationnelle
\item Théorème de Kleene: Tout langage rationnel est reconnaissable
\item Lex permet de programmer des  analyseurs lexicaux en s'affranchissant totalement des problèmes de création et de gestion des automates
\end{itemize}


\begin{tikzpicture}

\node (source) {Programme source lex};
\node[rectangle,draw, below=of source] (compilo) {Compilateur lex};
\node[below=of compilo] (prog) {Programme source C};
\node[rectangle,draw, below=of prog] (compilo2) {Compilateur C};
\node[below=of compilo2] (prog2) {Programme exécutable};

\path[->] 
(source) edge (compilo)
(compilo) edge (prog)
(prog) edge (compilo2)
(compilo2) edge (prog2);

\end{tikzpicture}

Exemple:

toto.l -> lex toto.l -> lex.yy.c -> gcc lex.yy.c -ll -> ./a.out
\subsection{Structure d'un programme lex}
\label{sec-5-2}

\begin{minted}[]{lex}
Déclarations

%%

Règles de traduction

%%

Fonctions auxiliaires
\end{minted}

\subsubsection{Déclarations}
\label{sec-5-2-1}

Cette partie permet de:
\begin{itemize}
\item déclarer des variables et des constantes en C. Ces déclarations doivent toutes êtres misent entre "\%\{" et "\%\}"
\item définir des "définitions régulières" qui permettent de donner des noms à des expression régulières qui seront utilisés dans le programme
\item Cette partie peut-être vide
\end{itemize}

\subsubsection{Règles de traduction}
\label{sec-5-2-2}

\begin{itemize}
\item De la forme:
\end{itemize}
\begin{minted}[]{lex}
m_1 { action 1 }
m_2 { action 2 }
...
m_n { action n }
\end{minted}

où les m$_{\text{i}}$ sont des expressions régulières lex et les actions sont des suites d'instructions en C.
Elles décrivent ce que l'analyseur lexical doit faire lorsqu'il reconnaît un lexème (un mot du texte) qui appartient au langage m$_{\text{i}}$ coresspondant.

\begin{enumerate}
\item Fonctionnement général
\label{sec-5-2-2-1}

\begin{itemize}
\item L'analyseur lexical produit par lex
\begin{itemize}
\item commence à chercher les lexèmes au début du code source
\item après chaque lexème reconnu, recommence à chercher les lexèmes juste après. (Exemple: si piraté est reconnu, raté n'est pas reconnu)
\end{itemize}
\item si aucun lexème n'est reconnu à partir d'un point du code source, l'analyseur affiche le premier caractère sur la sortie standard et recommence à chercher à partir du caractère suivant
\end{itemize}

\item Gloutonnerie et priorité
\label{sec-5-2-2-2}

\begin{itemize}
\item lex est glouton: il essaie toujours de reconnaître le mot le plus long possible
\item lorsqu'un même mot peut correspondre à deux expressions m$_{\text{i}}$ différents, c'est la première qui est prise en compte
\item lorsqu'un caractère ne fait partie d'aucune expression m$_{\text{i}}$, il est affiché sur la sortie standard
\end{itemize}

\item Expressions régulières en lex
\label{sec-5-2-2-3}

Alphabets: codes ISO, ASCII, etc   

\begin{center}
\begin{tabular}{ll}
\hline
Opérations rationnelles & \\
\hline
\texttt{e?} & 0 ou 1 fois l'expression \texttt{e}\\
\texttt{e\textasciicircum{}*} & 0 ou \texttt{n} fois l'expression \texttt{e} (\texttt{n} quelconque)\\
\texttt{e+} & 1 ou \texttt{n} fois l'expression \texttt{e} (\texttt{n} quelconque)\\
\texttt{e f} & l'expression \texttt{e f}\\
\texttt{e  f} & l'expression \texttt{e} ou l'expression \texttt{f}\\
$e\{n,m\}$ & l'expression \texttt{e} répétée entre \texttt{n} et \texttt{m} fois\\
\texttt{(e)} & l'expression \texttt{e}\\
\texttt{\{D\}} & l'expression obtenue par substitution de D (macro)\\
\hline
Sensibilité au contexte & \\
\hline
\texttt{e / f} & l'expression $e$ si suivie de l'expression $f$\\
\texttt{\textasciicircum{}e} & l'expression $e$ en début de ligne\\
\texttt{e\$} & l'expression $e$ en fin de ligne\\
\texttt{<E> e} & l'expression $e$ si dans l'état $E$\\
\hline
Caractères & \\
\hline
\texttt{.} & tout caractère sauf \texttt{\textbackslash{}n}\\
\texttt{\textbackslash{}c} & la caractère \texttt{c}, même spécial\\
$"abc"$ & la chaine de caractères $abc$\\
\texttt{[abc]} & le caractère $a$ ou $b$ ou $c$\\
\texttt{[\textasciicircum{}abc]} & un des caractères sauf $a$, $b$, $c$\\
\texttt{[a-c]} & le caractère $a$ ou $b$ ou $c$\\
\hline
\end{tabular}
\end{center}

\item Fonctions auxiliaire
\label{sec-5-2-2-4}

\begin{itemize}
\item Ce sont des fonctions C qui sont utilisées pour l'analyse lexicale. En général, cette partie contient la fonction main().
\item Cette partie peut-être vide, dans ce que le code par défaut est:
\end{itemize}
\begin{minted}[]{lex}
main() {
   yylex()
}
\end{minted}

\begin{itemize}
\item La fonction \texttt{yylex()} est la fonction "standard" d'analyse lexicale. Son rôle est de lire le texte en entrée standard lettre par lettre et d'appliquer les règles définies dans la partie 2 du programme lex.
\end{itemize}


\item Variables spéciales de lex
\label{sec-5-2-2-5}

\begin{center}
\begin{tabular}{ll}
Nom & Description\\
\hline
yytext & contient le lexème en cours de traitement\\
yyleng & longeur du yytext\\
yylval & la valeur du lexème si c'est une valeur numérique\\
yylineno & numéro de ligne du lexème\\
\hline
\end{tabular}
\end{center}
\end{enumerate}

\section{Grammaires formelles}
\label{sec-6}

Les grammaires permettent de définir des classes de langages plus générales que les automates et les expressions rationnelles.
Elle permettent de mettre en oeuvre l'analyse syntaxique. C'est la deuxième étape de la compilation.

\subsection{Système de réécriture}
\label{sec-6-1}

\begin{itemize}
\item Soit $A$ un alphabet. On appelle système de réécriture sur $A^*$ tout partie de $A^* \times A^*$.
\item Soit $R$ un système de réécriture sur $A^*$, soient $f$ et $g$ deux mots de $A^*$. On dit que $f$ se réécrit en $g$ si et seulement
\end{itemize}
s'il existe $(u,v) \in R, \alpha \in A^*, \beta \in A^*$ tels que $f = \alpha u \beta$ et $g = \alpha v \beta$
\begin{itemize}
\item On note $f \rightarrow_R g$ (ou simplement $f \rightarrow g$ s'il n'y a pas d'ambiguïté sur R).
\end{itemize}

Exemple:

\begin{itemize}
\item $R = \{(a,ab),(b,ba),(ab,bb),(abb,\epsilon)\}$
\item On a : $aab \rightarrow abab \rightarrow abbb \rightarrow b \rightarrow ba \rightarrow bab$
\end{itemize}

\subsection{Grammaire}
\label{sec-6-2}

Une grammaire est un quadruplet \$G = <A,V,P,S>, où
\begin{itemize}
\item $A$ (fini) est l'alphabet terminal
\item $V \cap A = \emptyset$ $V$ (fini) est l'alphabet non terminal
\item $P$ est un système de réécriture sur $(A \cup V^*)$ tel que chaque membre gauche contient au moins une lettre de $V$. C'est l'ensemble (fini) des règles (ou productions) de $G$.
\item $S \in V$. C'est l'axiome de $G$
\end{itemize}


\begin{mydef}
Soient $f$ et $g$ deux mots de $(A \cup V)^*$. On dit que $f$ se dérive (directement) en $g$ si $f \rightarrow_p g$.
\end{mydef}

\begin{mydef}
Une suite de dérivations est une suite de mots $f_0$,$f_1$,...,$f_n$ tels que $f_0 \rightarrow f_1 \rightarrow ... \rightarrow f_n$
\end{mydef}

\begin{mydef}
$n$ est la longueur de la suite de dérivations
\end{mydef}

\begin{mydef}
On note $f \rightarrow^* g (f \rightarrow^n g)$ s'il existe une sute de $(n)$ dérivations de $f$ à $g$. On dit que $f$ se dérive (à l'ordre $n$) en $g$.
\end{mydef}


Pour tout mot $f$, on a $f \rightarrow^* f$

\subsection{Conventions de notation}
\label{sec-6-3}

\begin{itemize}
\item Les symboles terminaux sont en minuscules, les symboles non terminaux en majuscules
\item Le premier symbole rencontré est l'axiome
\item Le "$\vert$" sépare les productions ayant le même membre gauche.
\end{itemize}

Exemple:

\begin{itemize}
\item $A = \{a,b\}$
\item $V = \{S, T\}$
\item $P = \{(S,aS);(S,bT);(S,\epsilon);(T,bS);\}$
\item Axiome : $S$
\end{itemize}

Notation usuelle:
$S \rightarrow aS \vert bT \vert \epsilon$
$T \rightarrow bS$

\subsection{Langage engendré par une grammaire}
\label{sec-6-4}

le langage engendré par une grammaire $G = <A, V, P, S>$ noté $L(G)$ est l'ensemble des mots $w \in A^*$ tels que $S \rightarrow^* w$.

\subsubsection{Questions}
\label{sec-6-4-1}

Montrez comment engendrer
\begin{itemize}
\item le mot "abbbba" avec la grammaire
\item $A = \{a,b\}$
\item $V = \{S, T\}$
\item $P = \{(S,aS);(S,bT);(S,\epsilon);(T,bS);\}$
\item Axiome : $S$

\item le mot "aacccbb" avec la grammaire
\item $S \rightarrow aSb \vert T$
\item $T \rightarrow cT \vert \epsilon$

\item le mot "aabbcc" avec la grammaire
\item $S \rightarrow aSBC \vert aBC$
\item $CB \rightarrow CD$
\item $CD \rightarrow BD$
\item $BD \rightarrow BC$
\item $aB \rightarrow ab$
\item $bB \rightarrow bb$
\item $bC \rightarrow bc$
\item $cC \rightarrow cc$
\end{itemize}

On peut démontrer que
\begin{itemize}
\item $L(G_1) = (a | bb)^*)$
\item $L(G_2) = \{a^n c^m b^n | n,m \geq 0 \}$
\item $L(G_3) = \{a^n b^n c^n | n > 0 \}$
\end{itemize}

\subsection{La hiérarchie de Chomsky}
\label{sec-6-5}

\begin{itemize}
\item Définit quatre types de grammaires selon des contraites que l'on impose ou pas sur la forme des règles
\item Chaque type de grammaires correspond à une classe de langages
\item Hiérarchie car il y a une relation d'inclusion stricte entre les quatre classes.

\item Grammaires générales (type 0): aucune restriction sur les règles de $P$

\item Grammaires contextuelles (type 1): toute règles de $P$ est de la frome suivante: $u N v \rightarrow u w v$ où $N \in V, u,v,w \in (A \cup V)^*$ et $w \neq \epsilon$.
\end{itemize}
On autorise cependant la règle $S \rightarrow \epsilon$ si $S$ est l'axiome, à condition que $S$ n'apparaisse pas dans le membre droit d'une règle.

Exemple:

$A \rightarrow aBb | cBd | \epsilon$
$aBb \rightarrow acBdb | acdb$
$cBd \rightarrow caBbd | cabd$

\begin{itemize}
\item Grammaire hors contexte ou algébrique (type 2): toute règle de $P$ est de la forme suivante $N \rightarrow w$ où $N \in V, w \in (A \cup V)^*$
\end{itemize}

\$ X $\rightarrow$ aYb | cZd | $\epsilon$\$
$Y \rightarrow cZd | cd$
$Z \rightarrow aYb | ab$

\begin{itemize}
\item Grammaires régulières (type 3):
\begin{itemize}
\item Soit toute règle de $P$ est dans l'une des formes suivantes (grammaire régulièree droite):
$N \rightarrow x N'$
$N \rightarrow x$
où $N \in V, x \in A \cup \{\epsilon\}$

Exemple:

$S \rightarrow aS | bT | \epsilon$
$T \rightarrow aS | a$

\item Soit toute règle de $P$ est dans l'une des formes suivantes (grammaire régulière gauche):
$N \rightarrow N' x$
$N \rightarrow x$
où $N \in V, x \in A \cup \{\epsilon\}$
\end{itemize}
\end{itemize}

Exemple:

$S \rightarrow Sa | Tb | \epsilon$
$T \rightarrow Sa | a$


\begin{itemize}
\item On dit qu'un langage est de type $i$ s'il existe une grammaire de type $i$ qui l'engendre
\end{itemize}
\begin{prop}
si un langage est de type $i$, il est aussi de type $j$ pour tout $j \leq i$
\end{prop}

\begin{prop}
la classe des langages de type 3 est la classe des langages reconnaissables.
\end{prop}

\subsubsection{Questions}
\label{sec-6-5-1}

\begin{itemize}
\item De quels types sont les grammaires des questions précédentes?
\item Ecrire une grammaire algébrique qui engendre le langage $\{a^n b^m | n \geq ; m \geq n \}$
\end{itemize}


\begin{tikzpicture}
\draw (0, 0) ellipse (3cm and 1cm)node[below=0cm] {Langages rationnels (type 3)};
\draw (0, 0) ellipse (5cm and 2cm) node[below=1cm] {Langages hors contexte (type 2)};
\draw (0, 0) ellipse (7cm and 3cm) node[below=2cm] {Langages contextuels (type 2)};
\draw (0, 0) ellipse (9cm and 4cm) node[below=3cm] {Langages récursivement énumérables (type 0)};
\end{tikzpicture}


L'inclusion entre les classes est stricte. Notamment:
\begin{itemize}
\item \$\{a$^{\text{n}}$ b$^{\text{n}}$ | n $\ge$ 0\} est l'algébrique mais pas rationnel
\item \$\{a$^{\text{n}}$ b$^{\text{n}}$ c$^{\text{n}}$ | n $\ge$ 0 \} est contextuel mais pas algébrique.
\end{itemize}

\subsection{Les langages hors contexte ou algébriques}
\label{sec-6-6}

\subsubsection{Arbre de dérivation}
\label{sec-6-6-1}

Soit \$G = <A,V,P,S> une grammaire hors contexte. On appelle arbre de dérivation sur $G$ tout arbre étiqueté sur $A \cup V \cup \{\epsilon\}$ tel que:
\begin{enumerate}
\item Si un noeud étiqueté $x$ a ses fils étiquetés (dans cet ordre) $y_1$,$y_2$,\ldots{}$y_k$, alors \$x $\rightarrow$ y$_{\text{1y}}$$_{\text{2}}$\ldots{}y$_{\text{k}}$ est une règle de $P$
\item Un noeud ne peut être étiqueté par $\epsilon$ que s'il est fils unique
\end{enumerate}

$S \rightarrow aSbS | T$ 

\$T $\rightarrow$ cT | $\epsilon$ \$

\begin{tikzpicture}[level/.style={sibling distance=25mm/#1}]
\node {S} 
  child { node {a} 
  }
  child { node {S} 
    child { node {a}
    }
    child { node {S}
      child { node {T}
        child { node {c}
        }
        child { node {T}
          child { node {$\epsilon$}
          }
        }
      }
    }
    child { node {b}
    }
    child { node {S}
      child { node {T}
        child { node {$\epsilon$}
        }
      }
    }
  }
  child { node {b} 
  }
  child { node {S}
    child { node {T}
      child { node {c}
      }
      child { node {T}
        child { node {$\epsilon$}
        }
      }
    }
  }
\end{tikzpicture}


\begin{prop}
Soit $G$
\end{prop}
% Emacs 24.4.1 (Org mode 8.2.10)
\end{document}
